<?php

$eet = "/usr/bin/eet";

function get_ip()
{
    if (getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
    else $ip = "UNKNOWN";
    return $ip;
}

ob_start();
############ limit - 20 mb.
$data = file_get_contents('php://input', NULL, NULL, 0, 50 * 1024 * 1024);
############ magic jpeg signature
$edj_match = "\x1e\xe7\x0f\x42";
$edj_magic = substr($data, 0, 4);

############ base on signaure, add file extension
if ($edj_match != $edj_magic) {
  header("HTTP/1.1 400 Bad Request");
  echo "Invalid File Format";
  ob_end_flush();
  die();
}

############ get a unique name
$dest = uniqid("e-", true);
$temp    = $_SERVER["DOCUMENT_ROOT"] . "/edjshare/tmp/" . $dest . ".edj";
$thumb   = $_SERVER["DOCUMENT_ROOT"] . "/edjshare/tmp/" . $dest . ".png";
$temp_ip = $_SERVER["DOCUMENT_ROOT"] . "/edjshare/tmp/ip-" . $dest;
$meta    = $_SERVER["DOCUMENT_ROOT"] . "/edjshare/tmp/meta-" . $dest . ".meta";

mkdir($_SERVER["DOCUMENT_ROOT"] . "/edjshare");
mkdir($_SERVER["DOCUMENT_ROOT"] . "/edjshare/tmp");

############ store the file
$fh = fopen($temp, 'wb');
fwrite($fh, $data);
fclose($fh);

############ extract thumb from edj and remove it
shell_exec($eet . " -x " . $temp . " edjshare/thumb " . $thumb);
shell_exec($eet . " -r " . $temp . " edjshare/thumb");
shell_exec($eet . " -x " . $temp . " edjshare/meta " . $meta);
shell_exec($eet . " -r " . $temp . " edjshare/meta ");

$meta_data = file_get_contents($meta);
$meta_data = preg_replace('/[^A-Za-z0-9\s]/', '', $meta_data);
$meta_data = preg_replace('!\s+!', ' ', $meta_data);
$meta_data = trim($meta_data, " ");

############ store ip of uploader
$fh = fopen($temp_ip, 'w');
fwrite($fh, md5($dest . get_ip()));
fclose($fh);
############ prepare url to get file from
$loc = "http://www.enlightenment.org/edjshare/" . $dest;

if (!file_exists($thumb) || !file_exists($meta) ||
    !rename($thumb, $_SERVER["DOCUMENT_ROOT"] . "/edjshare/" . $dest . ".png"))
{
    unlink($temp);
    unlink($thumb);
    unlink($temp_ip);
    unlink($meta);
    header("HTTP/1.1 400 Bad Request");
    if (!file_exists($thumb)) echo "No Thumb";
    else if (!file_exists($meta)) echo "No meta";
    else echo "Invalid File Format";
    ob_end_flush();
    die();
}

rename($temp, $_SERVER["DOCUMENT_ROOT"] . "/edjshare/" . $dest . ".edj");
rename($temp_ip, $_SERVER["DOCUMENT_ROOT"] . "/edjshare/ip-" . $dest);
file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/edjshare/index.txt",
                  $dest . " " . $meta_data . "\n", FILE_APPEND | LOCK_EX);
unlink($meta);

############ respond!
header("HTTP/1.1 200 OK");
header("Content-Type: text/plain");
header("X-Enlightenment-Service: Pants On");
print $loc;
ob_end_flush();
?>
