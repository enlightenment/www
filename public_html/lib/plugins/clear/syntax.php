<?php
/**
 * Clear plugin
 * 
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Ikuo Obataya<I.Obataya@gmail.com>
 */
if(!defined('DOKU_INC')) die();
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');
 
class syntax_plugin_clear extends DokuWiki_Syntax_Plugin {
    var $boxmode = 'left';
    function getInfo(){
    return array(
      'author' => 'Ikuo Obataya',
      'email'  => 'ikuo_obataya@symplus.co.jp',
      'date'   => '2007-08-6',
      'name'   => 'Clear plugin',
      'desc'   => 'Clears the alignments of div sections',
      'url'    => 'http://wiki.symplus.co.jp/computer/en/clear_plugin',
      );
    }
    function getType() { return 'substition'; }
    function getSort() { return 32; }

    function connectTo($mode) {
        $this->Lexer->addSpecialPattern('<clear/>',$mode,'plugin_clear'); 
    }
    function handle($match, $state, $pos, Doku_Handler $handler){ return array($match, $state, $pos); }
    function render($mode, Doku_Renderer $renderer, $data) {
       if($mode == 'xhtml'){
          $renderer->doc .= "<div style='clear:both;'></div>";
          return true;
      }
      return false;
    }
}
