<?php
/**
 * DokuWiki Plugin codelink (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  Carsten Haitzler <raster@rasterman.com>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();

class syntax_plugin_codelink extends DokuWiki_Syntax_Plugin {

    var $keydata = array();
    var $keylanguage;

    function load_lang_data($language) {
        global $conf;
        if (!empty($this->keydata[$language])) {
            return;
        }

        $extn_dir = $conf['datadir'] . '/' . $conf['code_extn'];
        if (!file_exists($extn_dir)) {
            $extn_dir = '';
        }
        else {
            $extn_link = $extn_dir . '/' . $language . '/keyword-link.txt';
            $extn_list = $extn_dir . '/' . $language . '/keyword-list.txt';
            if (file_exists($extn_link) && file_exists($extn_list)) {
                $link_array = file($extn_link, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $keys_array = file($extn_list, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $this->keydata[$language]['link'] = $link_array[0];
                $this->keydata[$language]['keys'] = $keys_array;
            }
        }
    }
    /**
     * @return string Syntax mode type
     */
    public function getType() {
        return 'substition';
    }
    /**
     * @return string Paragraph type
     */
    public function getPType() {
        return 'normal';
    }
    /**
     * @return int Sort order - Low numbers go before high numbers
     */
    public function getSort() {
        return 999;
    }

    /**
     * Connect lookup pattern to lexer.
     *
     * @param string $mode Parser mode
     */
    public function connectTo($mode) {
        global $conf;
        $langs = opendir($conf['datadir'] . '/' . $conf['code_extn']);
        if ($langs === false)
            return;

        while (false !== ($l = readdir($langs))) {
            if ($l == "." || $l == "..") {
                continue;
            }

            $this->Lexer->addSpecialPattern('~~CODE-' . $l . '~~', $mode, 'plugin_codelink');
            $this->load_lang_data($l);
        }

        $this->Lexer->addSpecialPattern('[\w-]+\(?\)?', $mode, 'plugin_codelink');
        closedir($langs);
    }

    /**
     * Handle matches of the codelink syntax
     *
     * @param string $match The match of the syntax
     * @param int    $state The state of the handler
     * @param int    $pos The position in the document
     * @param Doku_Handler    $handler The handler
     * @return array Data for the renderer
     */
    public function handle($match, $state, $pos, Doku_Handler $handler){
        $data = array();

        if (preg_match('/^~~CODE-/', $match)) {
            // find special codelang tag and find declared language in it
            // i.e. ~~CODE-c~~ -> c
            // ~~CODE-py~~ -> py
            // ~~CODE-lua~~ -> lua
            // ~~CODE-cpp~~ -> cpp
            $language = str_replace('~~CODE-', '', $match);
            $language = str_replace('~~', '', $language);
            $this->keylanguage = $language;
        } else {
            $symbol = str_replace("()", "", $match);
            if (!$this->keydata[$this->keylanguage]['keys'] ||
                    !in_array($symbol, $this->keydata[$this->keylanguage]['keys'])) {
                $data[0] = $match;
                return $data;
            }

            $data[0] = '<code class="code"><a href=' .
              str_replace
              (
               array(
                     '{FNAME}',
                     '{FNAMEL}',
                     '{FNAMEU}'),
               array(
                     str_replace('+', '%20', urlencode($symbol)),
                     str_replace('+', '%20', urlencode(strtolower($symbol))),
                     str_replace('+', '%20', urlencode(strtoupper($symbol))),
                     ),
               $this->keydata[$this->keylanguage]['link']) .
              '><span class="kw5">' . $match . '</span></a></code>';
        }
        return $data;
    }

    /**
     * Render xhtml output or metadata
     *
     * @param string         $mode      Renderer mode (supported modes: xhtml)
     * @param Doku_Renderer  $renderer  The renderer
     * @param array          $data      The data from the handler() function
     * @return bool If rendering was successful.
     */
    public function render($mode, Doku_Renderer $renderer, $data) {
        if ($mode != 'xhtml') return false;
        $renderer->doc .= $data[0];
        return true;
    }
}

// vim:ts=4:sw=4:et:
