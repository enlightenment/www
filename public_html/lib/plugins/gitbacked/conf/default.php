<?php
/**
 * Default settings for the gitbacked plugin
 *
 * @author Wolfgang Gassler <wolfgang@gassler.org>
 */

$conf['pushAfterCommit'] = 0;
$conf['periodicPull'] = 0;
$conf['periodicMinutes'] = 60;
$conf['commitPageMsg']	= 'Wiki page %page% changed with summary [%summary%] by %user%';
$conf['commitPageMsgDel']	= 'Wiki page %page% deleted with reason [%summary%] by %user%';
$conf['commitMediaMsg']	= 'Wiki media %media% uploaded by %user%';
$conf['commitMediaMsgDel']	= 'Wiki media %media% deleted by %user%';
$conf['repoPath']	= $GLOBALS['conf']['savedir'];
$conf['repoWorkDir']	= $GLOBALS['conf']['savedir'];
$conf['addParams'] = '';
$conf['ignorePaths'] = '';

/* plugin/gitbacked: Define a nice author (until addParams is fixed)
 * MUST NOT be defined in conf/local.php as $GLOBALS['USERINFO'] does not yet exist there
 */
$_git_auhor_name = 'www.enlightenment.org';
$_git_auhor_mail = 'no-reply@enlightenment.org';

if (isset($GLOBALS['USERINFO']['name'])) {
  $_git_auhor_name = $GLOBALS['USERINFO']['name'];
}

if (isset($GLOBALS['USERINFO']['mail'])) {
  $_git_auhor_mail = $GLOBALS['USERINFO']['mail'];
}

putenv("GIT_AUTHOR_NAME='$_git_auhor_name'");
putenv("GIT_AUTHOR_EMAIL='$_git_auhor_mail'");

/* plugin/gitbacked: Let access to ssh-agent */
putenv("HOME=" . dirname($_SERVER['DOCUMENT_ROOT']));
putenv("SSH_AUTH_SOCK=" . dirname($_SERVER['DOCUMENT_ROOT']) . "/.ssh/agent");
