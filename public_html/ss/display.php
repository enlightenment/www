<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
  <head>
    <title>Enlightenment Screenshots</title>
    <link href="index.css" rel="stylesheet" type="text/css"></link>
  </head>
  <?php
   function get_ip()
   {
     if (getenv("REMOTE_ADDR")) $ip = getenv("REMOTE_ADDR");
     else $ip = "UNKNOWN";
     return $ip;
   }

   $img = "";
   if (isset($_GET['image']))
     $img = $_GET['image'];

   $auth_file = $_SERVER["DOCUMENT_ROOT"] . "/ss/ip-" . $img;
   $auth_expire = 60 * 60; // You have one hour to remove your content

   if (file_exists($auth_file) &&
       time() - filemtime($auth_file) < $auth_expire)
     {
       $auth = md5($img . get_ip());

       $fh = fopen($auth_file, "r");
       $head = fgets($fh);
       fclose($fh);
     }
   else
     {
       $auth = false;
       $head = true;
     }

   print "<center><a href=http://www.enlightenment.org/ss/" . $img . ">";
   print "<img src=" . $img . " class=full>";
   print "</a><br>\n";
   print "</center>\n";

   ?>
  </body>
</html>
